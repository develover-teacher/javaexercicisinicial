package example7;

import java.util.Scanner;

/**Donada una cadena, extreure la quarta i cinquena lletra usant el mètode substring.
 *
 * @author alumne
 */
public class Example7 {
   public static void main(String[] args) {
        Scanner sscanner = new Scanner (System.in);
        String v_oracion_o,v_result;
        System.out.println ("Escribe algo: ");
        v_oracion_o = sscanner.nextLine (); 
        //Controlo el tamaño de de la string para hacer control de errores en caso de strings cortas.
        if (v_oracion_o.length()>5){
            v_result = v_oracion_o.substring(3,5);
            System.out.println("El resultado es: "+ v_result);      
        }
        else{
            System.out.println("La String no es suficientemente larga");
        }
    } 
}
