/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examole4Student;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

/**Crear una aplicació que ens permet inserir nombres fins que inserim un -1. 
 * Calcular el total de nombres introduïts, quin és el més petit, quin és el més gran, i el més repetit.
 *
 * @author alumne
 */
public class Example4Student {
    static Map<Double,Integer> m= new HashMap<Double,Integer>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        double intro;
        
        List nombres=new ArrayList();
        
        //lectura dels valors introduïts
        Scanner in=new Scanner(System.in);
        System.out.println("Escriu números. Acaba insertant un -1");
        
        intro=in.nextDouble();
        while(intro!=-1){
            
            nombres.add(intro);
            intro=in.nextDouble();
           
        }
         calculaFrequencies(nombres);		
         calculaMinim(nombres);
         calculaMaxim(nombres);
      
       
        System.out.println("Has introduït els següents valors");
        System.out.println(m);
        calculaModa(m);
        
        //int repeticions=Collections.max(m.values());
       // System.out.println(repeticions);
    
}
/**A partir d'una mmista es crea un Map que emmagatzema els valors i les seves repeticions
 * 
 * @param nombres llistat dels números introduïts per teclat
 */
    private static void calculaFrequencies(List nombres) {
        for (int i=0;i<nombres.size();i++){			
            if (m.containsKey(nombres.get(i))){
         
                m.put((Double) nombres.get(i), m.get(nombres.get(i))+1);
            }else
                m.put((Double)nombres.get(i),1);			
            }
         }
/**A partir de la llista de nombres, i gràcies a Collections podem calcular el màxim sense esforços
 * 
 * @param nombres llistat dels números introduïts per teclat
 */
    private static void calculaMaxim(List nombres) {
       
        System.out.println("El màxim és");
        System.out.println(Collections.max(nombres));
    
    }
/**A partir de la llista de nombres, i gràcies a Collections podem calcular el mínim sense esforços
 * 
 * @param nombres llistat dels números introduïts per teclat
 */
    private static void calculaMinim(List nombres) {
        System.out.println("El mínim és");
        System.out.println(Collections.min(nombres));
    }
/**A partir del map i mirant el màxim de les repeticions es calcula la key a la que correspon
 * @see #calculaFrequencies(java.util.List)
 * @param m Map creat al mètode calculaFrequencies
 */
    private static void calculaModa(Map<Double, Integer> m) {
        Iterator it = m.keySet().iterator();
        double repeticions=Collections.max(m.values());
        while(it.hasNext()){
            Double key = (Double) it.next();
            if(m.get(key)==repeticions){
                System.out.println("La moda la té: "+key);
                break;
            }
            
           
        } 
    }
}
