
package example10;

import java.util.Scanner;

/**
 *
 * @author alumne
 */
public class example10 {
  public static void main(String[] args) {  
 Scanner lector = new Scanner(System.in);

        String selJugador;
        String maquina;
        int opcion = 1;

        while (opcion != 2) { //este while es para que el usuario juegue las veces que quiera
            //pedir al usuario su opcion
            System.out.print("Ingrese, Piedra, Papel o Tijera: ");
            selJugador = lector.nextLine();

            System.out.println("Usuario VS Maquina");
            //torn de la màquina
            maquina = juegoMaquina();
            //llamo a la funcion juego que es donde se realiza las comparaciones de el usuario y la maquina
            juego(selJugador, maquina);

            System.out.print("Quieres seguir jugando 1 Si 2 No: ");
            opcion = lector.nextInt();
            lector.nextLine();
        }
    }

  /**Saco un numero aleatorio y a ese le asigno piedra papel o tijera
   * 
   * @return piedra papel o tijera aleatorio
   */
    public static String juegoMaquina() {
        int numAleatorio;
        String jugada;

        // un numero del 1 al 3 y a ese numero asignarle un valor 
        numAleatorio = (int) (Math.random() * 3) + 1;

        if (numAleatorio == 1) {
            jugada = "piedra";
        }
        if (numAleatorio == 2) {
            jugada = "papel";
        } else {
            jugada = "tijera";
        }
        return jugada;
    }

    
    /**Se realiza las comparaciones de el usuario y la maquina
     * 
     * @param usuario valor cridat per teclat
     * @param maquina valor aleatori que simula la màquina
     */
    public static void juego(String usuario, String maquina) {

        System.out.println(usuario + "    " + maquina);
        //comparaciones de cada estring para cada caso posible y imprimirlo por pantalla pata ver el resultado del juego
        if (usuario == null ? maquina == null : usuario.equals(maquina)) {
            System.out.println("Empate, ambos escogieron: " + usuario);
        } else {
        }
        if ("piedra".equals(usuario) && "papel".equals(maquina)) {
            System.out.println(usuario + " - " + maquina + " Gana la Maquina");
        }
        if ("piedra".equals(usuario) && "tijera".equals(maquina)) {
            System.out.println(usuario + " - " + maquina + " Gana el usuario");
        }
        if ("papel".equals(usuario) && "tijera".equals(maquina)) {
            System.out.println(usuario + " - " + maquina + " Gana la Maquina");
        }
        if ("papel".equals(usuario) && "piedra".equals(maquina)) {
            System.out.println(usuario + " - " + maquina + " Gana el usuario");
        }
        if ("tijera".equals(usuario) && "piedra".equals(maquina)) {
            System.out.println(usuario + " - " + maquina + " Gana la Maquina");
        }
        if ("tijera".equals(usuario) && "papel".equals(maquina)) {
            System.out.println(usuario + " - " + maquina + " Gana el usuario");
        }
    }
}   

