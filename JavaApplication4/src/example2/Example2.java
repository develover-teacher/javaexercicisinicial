
package example2;

import java.util.Scanner;

/**
 * Llegeix un nombre per teclat i indica si és divisible entre 2 (resta = 0). Si no ho és, també
 * hem de indicar-ho. En cas afirmatiu que calculi el factorial (n*(n-1)*...*1)
 * @version beta-male
 * @author alumne
 */
public class Example2 {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        int num1;
        
        System.out.println("Ingresa el teu primer numero");   
        num1=teclado.nextInt();    
                    
        if (num1%2==0) {
            System.out.println("El #"+num1+" es par\n");         
            System.out.println("El factorial es "+calculaFactorial(num1));
           
        } else{
            System.out.print("Es impar\n");         
        }
    
    } 
    public static double calculaFactorial(double num1){
        if (num1==0) return 1;
        else
        { 
            double resultat = num1 * calculaFactorial(num1 -1); 
            return resultat;
         }
    }
}
