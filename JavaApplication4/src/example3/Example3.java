package example3;

import java.util.Scanner;

/**
 * Llegeix un nombre per teclat que demani el preu d'un producte (pot tenir decimals) i calculi
 * el preu final amb IVA. L'IVA serà una constant que serà el 21%
 *
 * @author alumne
 */
public class Example3 {
    final static double IVA=0.21;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double precio, total;
        
        System.out.print("Dime el precio: ");
        precio = sc.nextFloat();
        
        total = precio+(precio*IVA);
        
        System.out.println("El precio total es: "+total);
    } 
}
