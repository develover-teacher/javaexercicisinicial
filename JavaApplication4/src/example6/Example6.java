/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example6;

import java.util.Scanner;

/**Convertir una frase a majúscules o minúscules, que donarem opció a que l'usuari ho demani
 *i mostrarem el resultat per pantalla
 *
 * @author alumne
 */
public class Example6 {
    public static void main(String[] args) {
        Scanner reader= new Scanner(System.in);
        
        System.out.println("Introduce la frase a cambiar: ");
        String frase1 = reader.nextLine();
        
        System.out.println("Escribe M para mayúscula o m para minúscula: ");
        String func = reader.nextLine();
        
        if (func.equals("M")) {
            System.out.println(frase1.toUpperCase());
        }else  if (func.equals("m")) {
            System.out.println(frase1.toLowerCase());
        }else{
            System.out.println("Has d'escriure una M o una m");
        }
        
    }
}
