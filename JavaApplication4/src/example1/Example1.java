package example1;

/** 
 * Declara 2 variables numèriques (amb el valor que desitgis), i indica 
 * quina és més gran de les dues. Si són iguals indicar-ho també.
 * @author aasensio
 *
 */
public class Example1 {
    public static void main(String[] args) {
        int n1 = 11, n2 = 7;
        if (n1 > n2) {
            printMessage("n1 és el més gran.");
        } else if (n2 > n1) {
            printMessage("n2 és el més gran.");
        } else {
            printMessage("n1 i n2 són iguals.");
        }
    } 
    
    /**Aquest mètode et retorna una missatge de texte
     * 
     * @param message Cadena a imprimir
     */
        
    private static void printMessage(String message){
        System.out.println(message);
    }
 
}